\version "2.19.80"
\language "italiano"

\include "blockflute1.ily"
\include "blockflute2.ily"

instrument = ""
\book {
  \include "header.ily"
  \score {
	<<
	  \new Staff \with {
		instrumentName = #"A"
		\consists "Ambitus_engraver"
	  } {\transpose do' sol\lamentoBlockfluteI}
	  \new Staff \with {
		instrumentName = #"A"
		\consists "Ambitus_engraver"
	  } {\transpose do' sol\lamentoBlockfluteII}
	>>
	\header {piece = "5. Il Lamento"}
	\layout { }
	\midi {\tempo 4 = 240}
  }
}

