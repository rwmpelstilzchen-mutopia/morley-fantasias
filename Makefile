all:
	lilypond -dno-point-and-click *.ly

distclean:
	-rm *.pdf *.midi
