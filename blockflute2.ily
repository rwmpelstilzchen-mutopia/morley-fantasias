\version "2.19.80"

lamentoBlockfluteII = {
  \transpose do do' {
	\key do \dorian
	\time 4/2
	\set Staff.midiInstrument = #"recorder"
	% 1/1
	r2 do'2. si4 do' re' |
	mib'4 do' mib'2 re'4 la re'4. do'8 |
	sib4 la sol sol'2 fad'8 mi' fad'2 |
	% 1/2
	sol'2 sol4 sol' fa' mib' mib' re'8 do' |
	re'1 do' |
	sol'2. fa'4 sol' la' sib' sol' |
	% 1/3
	la'4 sib' do''2 sib'4 sol' la'2 |
	la'1 sol'2 do' |
	do'2 do' sib4. la8 sol la sib sol |
	% 1/4
	la8 sib do'2 si4 do'2 sol' ~ |
	sol'4 fad'8 mi' fad'2 sol'4 mib' re'2 |
	do'1 r4 do' mib' fa' |
	% 1/5
	sol'4. la'8 sib'4 sol' fad' sol'2 fad'4 |
	sol'4 do' do' do' sib4. la8 sol8 la sib sol |
	la8 sib do'2 si4 do' do'' do'' do'' |
	% 1/6
	sib'4. la'8 sol'4 sib' la' sol' la'2 |
	sol'2 mib' do'4. sib8 do' re' mib' do' |
	re'4 mib'2 re'4 mib'2 sol' |
	% 2/1
	do'2. re'4 mib'2 do' |
	sol'2 r4 do''2 sib' la'4 ~ |
	la'4 sol'2 fad'4 sol' sib' la'2 |
	sol'4 re'4. mib'8 fa'4 mib' do'4. re'8 mib'4 |
	% 2/2
	re'8 do' sib la sol4 sol'2 fad'4 sol' fa'8 mib' |
	re'4 do' re'2 do'1 |
	r2 do''2 sol'2. la'4 |
	% 2/3
	sib'2 sol' do'' sib' |
	la'2 sol' fa' sib' |
	la'4 sol' la'2 sol' do'8 re' mib' fa' |
	% 2/4
	sol'8 la' sib' la' sol' fa' mib' re' do' re' mib'4 re' do' |
	si4 do'2 si4 do'2 sol' |
	do'2. re'4 mib'2 do' |
	% 2/5
	sol'4 sol2 do'4. re'8 mib' fa' sol'4 sol |
	do'4 re' mib' do' re'8 do' sib la sol4 sol' |
	fad'4 sol'2 fad'4 sol' do'8 re' mib' fa' sol' la' |
	% 2/6
	sib'8 la' sol' fa' mib' re' do' re' mib' fa' sol' la' sib'4 sol' |
	la'4 sib' do''2. si'8 la' si'2 |
	do''\breve |
	\bar "||"
  }
}
