\version "2.19.80"

lamentoBlockfluteI = {
  \transpose do do' {
	\key do \dorian
	\time 8/4
	\set Staff.midiInstrument = #"recorder"
	% 1/1
	sol'\breve ~ |
	sol'1  fad'1 |
	sol'1  la'1 |
	% 1/2
	sib'4 sol' sib'2  la'4 sol' do''2 ~ |
	do''4 si'8 la' si'2 do'' do' ~ |
	do'4 si do' re' mib' do' re' mib' |
	% 1/3
	fa'4 re' mib' fa' sol'2 sol4 sol' ~ |
	sol'4 fad'8 mi' fad'2 sol'1 |
	r2 sol' sol' sol' |
	% 1/4
	fa'4. mib'8 re'4 fa' mib' do' mib'2 |
	re'1 sol4 do'2 si4 |
	do'2 do'' do'' do'' |
	% 1/5
	sib'4. la'8 sol'4 sib' la' sol' la'2 |
	sol'2 r4 sol'4 sol'2 sol' |
	fa'4. mib'8 re'4 fa' mib' do' mib' fa' |
	% 1/6
	sol'4 sol8 la si do' re' mi' fad'4 sol'2 fad'4 |
	sol'4 sib'2 sol' lab' sol'4 |
	fa'4 mib' fa'2 mib'1 |
	\pageBreak
	% 2/1
	r2 sol' do'2. re'4 |
	mib'2 do' sol' do'' |
	sib'2 la' r4 sol'2 fa'4 |
	sib'2 la' sol' do' |
	% 2/2
	fa'2 sib' la' sol'4 do'' |
	si'4 do''2 si'4 do''2 sol' |
	do'2. re'4 mib'2 do' |
	% 2/3
	sol'4 sol2 do'4. re'8 mib' fa' sol'4 sol |
	do'4 re' mib' do' re'8 do' sib la sol4 sol' |
	fad'4 sol'2 fad'4 sol' do'8 re' mib' fa' sol' la' |
	% 2/4
	sib'8 la' sol' fa' mib' re' do' re' mib'4 do' fa' mib' |
	re'4 do' re'2 do'1 |
	r2 do'' sol'2. la'4 |
	% 2/5
	sib'2 sol' do'' sib' |
	la'2 sol' fa' sib' |
	la'4 sol' la'2 sol' do'8 re' mib' fa' |
	% 2/6
	sol'8 la' sib' la' sol' fa' mib' re' do' re' mib' do' re'4 mib' |
	fa'4 mib' mib' re'8 do' re'1 |
	do'\breve
	\bar "||"
  }
}
