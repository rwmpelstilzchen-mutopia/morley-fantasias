\version "2.19.80"
\language "italiano"

\include "blockflute2.ily"

instrument = "Blockflute II"
\include "header.ily"
\header {piece = "5. Il Lamento"}
\score {
  \new Staff {\lamentoBlockfluteII}
  \layout { }
  \midi {\tempo 4 = 240}
}
