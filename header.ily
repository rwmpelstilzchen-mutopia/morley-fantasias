\version "2.19.80"

\header {
  title = "Nine Fantasias"
  composer = "Thomas Morley"
  opus = ""
  date = ""
  source = ""
  style = "Renaissance"

  instrument = \instrument

  mutopiacomposer = "MorleyT"
  mutopiainstrument = "Recorder"

  maintainer = "Júda Ronén"
  maintainerWeb = "http://me.digitalwords.net/"
  license = "Public Domain"
  lastupdated = "2016-05-08"

  moreInfo = ""
}
