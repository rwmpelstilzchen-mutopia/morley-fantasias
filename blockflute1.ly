\version "2.19.80"
\language "italiano"

\include "blockflute1.ily"

instrument = "Blockflute I"
\include "header.ily"
\header {piece = "5. Il Lamento"}
\score {
  \new Staff {\lamentoBlockfluteI}
  \layout { }
  \midi {\tempo 4 = 240}
}
